<?php

class API {
    public function getFullName($fullName) {
        echo "Full Name: $fullName <br>";
    }

    public function getHobbies(array $hobbies) {
        echo "Hobbies:";
        foreach ($hobbies as $hobby) {
            echo "<br> &nbsp &nbsp &nbsp $hobby" . PHP_EOL;
        }
    }

    public function getDescription($description) {
        $age = $description['age'];
        $email = $description['email'];
        $birthday = $description['birthday'];
        $url = "www.linkedin.com/in/jimbryansantana";

        echo "<br>";
        echo "Age: $age <br>"; 
        echo "Email: <a href='url'> $email </a> <br>"; 
        echo "Birthday: $birthday <br>"; 
    }
}

$api = new API();

$api->getFullName("Jim Bryan Alimento Santana");

$hobbies = ["Playing Video Games", "Reading Manga/Manhwa/Manhua", "Listening Music", "Playing Guitar"];
$api->getHobbies($hobbies);
$description = [
    'age' => 21,
    'email' => 'santana.jimbryan@gmail.com',
    'birthday' => 'March 25, 2002'
];
$api->getDescription($description);
?>